# 3D Digital Clock

The aim of the project was to model a 3D clock using CSS, HTML, and JavaScript. 

Just download the respository and unzip the file. <br>
Open index.html file and here you go.<br>
Enjoy the index with 3D digital clock.<br>

![3d_digital_clock](/uploads/51b25aea8c9de07fd6f39c0f9bd07b50/3d_digital_clock.gif)

**Feel free to play with html, css, and JS. If you have a crazy idea, then pull request :) **

